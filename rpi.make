# This makefile is to compile code on Raspberry Pi
# Otherwise, make is managed by Eclipse

CC=gcc

all: CFLAGS=-DDATABASE_ENA -DGUI_INTERFACE_DB -DRADIO_BAUD_RATE_10000 -DRADIO_CC1101 -DSMS_ENA -O1 -g3 -Wall -fmessage-length=0
cloud: CFLAGS=-DDATABASE_ENA -DGUI_INTERFACE_DB -DRADIO_BAUD_RATE_10000 -DRADIO_CC1101 -DSMS_ENA -DREMOTE_DB -O1 -g3 -Wall -fmessage-length=0

INC=-I"./inc" -I"./inc/imports"
LIBS=-lpthread -lmysqlclient -lm
OBJDIR=Debug
EXE=Debug/wsuart

SRCS=$(wildcard src/*.c)
OBJS=$(SRCS:%.c=Debug/%.o)
INCS=$(wildcard inc/*)

all: odir $(EXE)

cloud: $(EXE)

$(EXE): $(OBJS)
	$(CC) $(CFLAGS) $(INC) $(OBJS) $(LIBS) -o $@

$(OBJDIR)/%.o : %.c $(INCS)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

odir:
	mkdir -p Debug/src

clean:
	rm -f $(OBJS) $(EXE)


