/*
 * main.c
 *
 *  Created on: Nov 13, 2013
 *      Author: AP
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <dis.h>
#include <gw.h>
#include <pktSched.h>
#include <pktProc.h>
#include <logger.h>
#include <util.h>
#include <wsuart.h>
#include <eventThread.h>
#include <main.h>

#include <database.h>
MYSQL *mainCon;
unsigned int NwInitDone = 0;

#ifdef SMS_ENA
#include <sms.h>
#endif

int main(int argc, char **argv)
{
	if (argc<2) {
		LOG(LOG_ERR, "UART device name is expected.");
		exit(1);
	}

	// UART configuration
	if (cfgPort(argv[1]) < 0) return 1;

#ifdef DATABASE_ENA
	// DB initializations
	initDb();
	mainCon = openDbConnection();
	createTables(mainCon);
	initTableVersions(mainCon); // TODO Update table versions during DB upgrade.
#endif

	initSchedTxQueue();

#ifndef SNIFFER
	while(startCoord()) sleep(1);
	scheduleNwInfo();
	while (numPktsQueued()) {
		processTxQueue();
	}
	initAllNodeInfo();
#ifdef DATABASE_ENA
	char *coordMacAddrStr = readCoordExtAddr(mainCon);
	UINT8_t *coordMacAddr = parseString(coordMacAddrStr);
	insertEvent(mainCon, coordMacAddr, "Info", "Boot", "Gateway finished initialization.");
	free(coordMacAddrStr);
	free(coordMacAddr);
#endif
#endif

#ifdef SMS_ENA
	GSM_initModem(GSM_modemSerDevName);
#endif

	// Create thread to handle incoming events
    pthread_t evtThread;
    thData evtData;
    pthread_create (&evtThread, NULL, (void *) &waitForEvents, (void *)&evtData);

    NwInitDone = 1; // now we are ready to process any packet

	// TODO Rather than polling, use interrupt driven rx and async tx
    while (1) {
        // Wait for packets and process them as received
    	UINT8_t *rxPkt = NULL;
		rxPkt = receivePkt();
		processPkt(NULL, rxPkt);
		free(rxPkt);

		// Process anything pending in transmit queue
		if (numPktsQueued()) { // only first packet
			processTxQueue();
		}
	}

#ifdef DATABASE_ENA
	closeDbConnection(mainCon);
	endDb();
#endif

	return 0;
}

