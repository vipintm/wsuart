/*
 * pktSched.c
 *
 *  Created on: Nov 23, 2013
 *      Author: AP
 */

#include <string.h>
#include <stdlib.h>
#include <gw.h>
#include <logger.h>
#include <pktProc.h>
#include <pktSched.h>

#define NUM_SCHED_TXPKTS 32
static SchedPktInfo TxQueue[NUM_SCHED_TXPKTS];
static UINT16_t headTxQ, countTxQ;

void initSchedTxQueue(void)
{
	memset(TxQueue, 0, sizeof(TxQueue));
	headTxQ = countTxQ = 0;
}

static UINT8_t addToQueue(UINT16_t type, UINT8_t *data, UINT16_t len)
{
	if (countTxQ==NUM_SCHED_TXPKTS) {
		LOG(LOG_ERR, "TxQueue is full. Could not schedule message 0x%04X", type);
		return 1;
	}

	UINT16_t i = (headTxQ+countTxQ) % NUM_SCHED_TXPKTS;

	if (len>sizeof(TxQueue[i].data)) {
		LOG(LOG_ERR, "Packet payload exceeds local queue storage. Could not schedule message 0x%04X", type);
		return 2;
	}

	TxQueue[i].type = type;
	TxQueue[i].len = len;
	if (data)
		memcpy(TxQueue[i].data, data, len);

	countTxQ++;

	return 0;
}

static SchedPktInfo* removeFromQueue(void)
{
	if (countTxQ==0) return NULL;

	SchedPktInfo *pkt = malloc(sizeof(SchedPktInfo));
	memcpy(pkt, &TxQueue[headTxQ], sizeof(SchedPktInfo));

	headTxQ = (headTxQ+1) % NUM_SCHED_TXPKTS;
	countTxQ--;

	return pkt;
}

UINT8_t numPktsQueued(void)
{
	return countTxQ;
}

void printQueue(const char *desc)
{
	UINT16_t i, j;

	printf("Scheduler Tx Queue [%s] (head,count): (%u,%u); (type,len): ", desc, headTxQ, countTxQ);
	for (i=0; i<countTxQ; i++) {
		j = (headTxQ+i) % NUM_SCHED_TXPKTS;
		printf("(%s,%u) ", getHdrTypeStr(TxQueue[j].type), TxQueue[j].len);
	}
	printf("\n");
}

UINT8_t scheduleHdr(UINT16_t type)
{
	return addToQueue(type, NULL, 0);
}

UINT8_t schedulePkt(UINT16_t type, UINT8_t *data, UINT16_t len)
{
	return addToQueue(type, data, len);
}

UINT8_t scheduleNwInfo(void)
{
	UINT8_t retVal = 0;
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_NWK_LPWMN_ID);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_NODE_CNT_REQ);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_RADIO_FREQ_BAND);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_RADIO_MOD_FMT);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_RADIO_BAUD_RATE);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_LPWMN_COORD_EXT_ADDR);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_RADIO_TX_PWR);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_NWK_JOIN_CTRL_STATE);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_DATE);
	retVal += scheduleHdr(LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_TIME);

	return retVal;
}

UINT8_t processTxQueue(void)
{
	SchedPktInfo *txPkt = removeFromQueue();
	if (txPkt==NULL) return 0;

	UINT8_t retVal = 0;
	UINT8_t *rxPkt = NULL;

	if (txPkt->len) { // header and payload
		rxPkt = txPktRxPkt(txPkt->type, txPkt->data, txPkt->len, 3);
	}
	else { // only header
		rxPkt = txHdrRxPkt(txPkt->type, 3);
		retVal += processPkt(NULL, rxPkt);
	}

	free(rxPkt);
	free(txPkt);

	return retVal;
}
