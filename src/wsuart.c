#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <wsuart.h>
#include <logger.h>

#define RX_TIMEOUT_SEC 0
#define RX_TIMEOUT_MSEC 300
static unsigned int timeout_sec=RX_TIMEOUT_SEC, timeout_ms=RX_TIMEOUT_MSEC;

static cntxt_s uart_cntxt =
{
  B38400, -1, NULL
};

void setDefaultTimeout()
{
	timeout_sec = RX_TIMEOUT_SEC;
	timeout_ms = RX_TIMEOUT_MSEC;
}

void setTimeout(unsigned int sec, unsigned int ms)
{
	timeout_sec = sec;
	timeout_ms = ms;
}

int readPort(unsigned char *buff_p, unsigned int len)
{
   int rdLen, readLeft = len, totRead = 0;
   fd_set fds;
   struct timeval tv;

   FD_ZERO(&fds);
   FD_SET(uart_cntxt.serialFd, &fds);

   while (readLeft > 0)
   {
      tv.tv_sec = timeout_sec; tv.tv_usec = timeout_ms*1000;
	  int numFd = select(uart_cntxt.serialFd+1, &fds, NULL, NULL, &tv);
      if (numFd <= 0 || !FD_ISSET(uart_cntxt.serialFd, &fds))
          return 0;

      rdLen = read(uart_cntxt.serialFd, buff_p + totRead, readLeft);
      LOG(LOG_DBG_LOW, "<%s> rdLen<%d>", __FUNCTION__, rdLen);
      if (rdLen > 0)
      {
          totRead += rdLen;
          readLeft -= rdLen;
      }
      else
      {
    	  LOG(LOG_DBG_LOW, "<%s> read() failed  - %d !!", __FUNCTION__, rdLen);
          return rdLen;
      }
   }

   return totRead;
}

int writePort(unsigned char *buff_p, unsigned int cnt)
{
   int rc, bytesLeft = cnt, bytesWritten = 0;
    
   LOG(LOG_DBG_LOW, "<%s> cnt<%d>", __FUNCTION__, cnt);
   
   while (bytesLeft > 0)
   {
      rc = write(uart_cntxt.serialFd, buff_p + bytesWritten, bytesLeft);
      if (rc <= 0)
          return -1;
      else
      {
          bytesLeft -= rc;
          bytesWritten += rc;
      }
   }

   return 1;
}
                                
int cfgPort(char *devName)
{
   cntxt_s *serialCntxt_p = &uart_cntxt;
   serialCntxt_p->devName_p = malloc(strlen(devName)+1);
   memset(serialCntxt_p->devName_p, 0, strlen(devName)+1);
   strncpy(serialCntxt_p->devName_p, devName, strlen(devName));

   serialCntxt_p->serialFd = open((char *)serialCntxt_p->devName_p, O_RDWR | O_NOCTTY | O_NDELAY);
   if (serialCntxt_p->serialFd < 0)
   {
	   LOG(LOG_FATAL, "<%s> open(%s) failed !! - errno<%d>",
              __FUNCTION__, serialCntxt_p->devName_p, errno);
       return -1;
   }
   
   // Zero out port status flags
   if (fcntl(serialCntxt_p->serialFd, F_SETFL, 0) != 0x0)
   {
       return -1;
   }

   bzero(&(serialCntxt_p->dcb), sizeof(serialCntxt_p->dcb));

   serialCntxt_p->dcb.c_cflag |= serialCntxt_p->baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= CLOCAL;  // local - don't change owner of port
   serialCntxt_p->dcb.c_cflag |= CREAD;  // enable receiver

   // Set to 8N1
   serialCntxt_p->dcb.c_cflag &= ~PARENB;  // no parity bit
   serialCntxt_p->dcb.c_cflag &= ~CSTOPB;  // 1 stop bit
   serialCntxt_p->dcb.c_cflag &= ~CSIZE;  // mask character size bits
   serialCntxt_p->dcb.c_cflag |= CS8;  // 8 data bits

   // Set output mode to 0
   serialCntxt_p->dcb.c_oflag = 0;
 
   serialCntxt_p->dcb.c_lflag &= ~ICANON;  // disable canonical mode
   serialCntxt_p->dcb.c_lflag &= ~ECHO;  // disable echoing of input characters
   serialCntxt_p->dcb.c_lflag &= ~ECHOE;
 
   // Set baud rate
   cfsetispeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);
   cfsetospeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);
 
   serialCntxt_p->dcb.c_cc[VTIME] = 0;  // timeout = 0.1 sec
   serialCntxt_p->dcb.c_cc[VMIN] = 1;
 
   if ((tcsetattr(serialCntxt_p->serialFd, TCSANOW, &(serialCntxt_p->dcb))) != 0)
   {
	   LOG(LOG_FATAL, "<%s> tcsetattr(%s) failed !! - errno<%d>",
              __FUNCTION__, serialCntxt_p->devName_p, errno);
       close(serialCntxt_p->serialFd);
       return -1;
   }

   // flush received data
   tcflush(serialCntxt_p->serialFd, TCIFLUSH);
   tcflush(serialCntxt_p->serialFd, TCOFLUSH);

   return 1;
}

