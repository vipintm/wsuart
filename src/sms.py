#!/usr/bin/env python

#======================================================================
# Description : Interface to GSM modem and send out an SMS
# Syntax      : sms.py <mobileNum> <message>
# Example     : sms.py 8123993894 "This is a test message"
#----------------------------------------------------------------------

import subprocess
import serial
import time
import sys
import re

#======================================================================
# Global config
#----------------------------------------------------------------------
GMI = 'Telit'


#======================================================================
# AtCmdInterface
#----------------------------------------------------------------------
class AtCmdInterface:
    def __init__(self, serialPort, baud=115200, timeout=1):
        self.serialPort = serialPort
        self.baud = baud
        self.timeout = timeout
        self.debug = {'rxSerial':True}
        
    def __enter__(self):
        self.ser = serial.Serial(port=self.serialPort, baudrate=self.baud, timeout=self.timeout)
        time.sleep(1)
        return self

    def __exit__(self, exType, value, traceback):
        self.ser.close()

    def rxRsp(self, cmd):
        """Read response and return the result as a tuple."""
        rsp = []
        while (True):
            line = self.ser.readline().strip()
            rsp.append(line)

            if self.debug['rxSerial']:
                print line
            
            if re.search(r'^(\+CM[ES] )?ERROR', line):
                sys.stderr.write('ERR: Error code returned for command "%s"\n' % cmd)
                return rsp
            elif line == 'OK' or line == '>':
                return tuple(rsp)

    def checkRsp(self, rsp, expectedStr, cmd, checkEcho=True):
        """Check the response against expected response for the command.
        Response is a tuple.
        Returns True if response is as expected."""
        if not rsp:
            sys.stderr.write('ERR: No response for command "%s"\n' % cmd)
            return False
        
        if checkEcho:
            if rsp[0]!=cmd:
                sys.stderr.write('ERR: Echo failed for command "%s"\n' % cmd)
                return False

        if rsp[-1]=="OK":
            if len(rsp)>2 and rsp[-2]:
                # OK or blank line followed by OK terminates the message
                sys.stderr.write('ERR: Unexpected response sequence for command "%s"\n' % cmd)
                return False
            
        # TODO Do we want to allow substring matches?
        if '\n' in expectedStr:
            # Handle a multiline string
            expLines = expectedStr.split('\n')
            for expLine in expLines:
                if not expLine in rsp:
                    sys.stderr.write('ERR: Expected string "%s" not found in response for command "%s"\n' % (expLine, cmd))
                    return False
        elif not expectedStr in rsp:
            sys.stderr.write('ERR: Expected string "%s" not found in response for command "%s"\n' % (expectedStr, cmd))
            return False
        
        return True

    def executeCmd(self, cmd, expectedStr="OK", checkEcho=True):
        """Execute command, receive response and check the response.
        Returns the response as a tuple regardless of checks."""
        self.ser.write('%s' % cmd)
        time.sleep(1)
        cmd = cmd.strip()
        rsp = self.rxRsp(cmd)
        self.checkRsp(rsp, expectedStr, cmd, checkEcho)
        return rsp

    def initModem(self):
        """Initialize the modem after the port has been opened."""
        self.executeCmd('ATZ\r')
        self.executeCmd('AT+CMGF=1\r')
        self.executeCmd('AT+GMI\r', GMI)
        self.executeCmd('AT+CPIN?\r', '+CPIN: READY')
        self.executeCmd('AT+CREG?\r', '+CREG: 0,1')
        
    def sendSms(self, recipient, msg):
        """Send an SMS message."""
        
        # TODO Remove limitations on message formatting.
        lines = msg.split('\n')
        for i, line in enumerate(lines):
            if line == 'OK':
                lines[i] = 'OK.'
        msg = '\n'.join(lines)
    
        self.executeCmd('AT+CMGS="%s"\r' % recipient, '>')
        self.executeCmd('%s\r%c' % (msg, chr(26)), msg, False)

    def receiveSms(self):
        """Run indefinitely in a loop waiting for SMS messages.
        Anything else received is discarded."""
        self.executeCmd('AT+CNMI=1,1,0,0,0\r')
        while (True):
            line = self.ser.readline().strip()

            if line:
                if self.debug['rxSerial']:
                    print line
                m = re.search(r'\+CMTI:\s*"SM",(\d+)', line)
                if m:
                    msgIndex = m.group(1)
                    rsp = self.executeCmd('AT+CMGR=%s\r' % msgIndex)
                    print self.parseSms(rsp)
                    self.executeCmd('AT+CMGD=%s\r' % msgIndex)

    def parseSms(self, rsp):
        """Parse an SMS message and retrieve its fields."""
        if len(rsp)<=4:
            return None

        msg = {}
        
        # Format of header: +CMGR: "REC UNREAD","+919731468194","","15/04/10,08:48:56+22"
        hdrFields = rsp[1].split('","')
        msg['callerNum'] = hdrFields[1]
        msg['callerName'] = hdrFields[2]
        msg['dateTime'] = hdrFields[3].strip('"')
        msg['body'] = rsp[2]

        return msg


#======================================================================
# Main code
#----------------------------------------------------------------------
p = subprocess.Popen('grep -l PRODUCT=67b/2303 /sys/bus/usb-serial/devices/tty*/../uevent | cut -d/ -f6', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
tty = p.stdout.readline().strip()
if not tty:
    # Expected SIM module not found
    exit(1)

tty = '/dev/%s' % tty
mobileNum = sys.argv[1]
msg = sys.argv[2]

with AtCmdInterface(tty) as txMsg:
    txMsg.initModem()
    txMsg.sendSms('+91%s' % mobileNum, msg)
    