/*
 * eventThread.h
 *
 *  Created on: Nov 14, 2013
 *      Author: AP
 */

#ifndef EVENTTHREAD_H_
#define EVENTTHREAD_H_

#include <pthread.h>

typedef struct
{
	int placeholder;
} thData;

#ifndef GUI_INTERFACE_TCPIP
typedef struct
{
	char type[32];
	char data[1024];
} PendingAction;
#endif

void waitForEvents(void *ptr);

#endif /* EVENTTHREAD_H_ */
