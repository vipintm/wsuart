/*
 * pktProc.h
 *
 *  Created on: Nov 13, 2013
 *      Author: AP
 */

#ifndef PKTPROC_H_
#define PKTPROC_H_

#include <typedefs.h>

#define PAN_COORD_SHORT_ADDR 0x0001
#define NODE_DEFAULT_PUSH_PERIOD_SEC 3
#define STR_LEN 32
#define VAR_PYLD_SIZE 0xFFFF
#define BASE_FREQ 865199829
#define CHANNEL_SPACING 199951
#define DEFAULT_CHANNEL_NUM 1

typedef struct
{
	UINT16_t panId;
	UINT16_t nodeCount;
	UINT8_t band[STR_LEN];
	UINT32_t channel;
	UINT8_t modulation[STR_LEN];
	UINT32_t radioBaudRate;
	UINT8_t coordMacAddr[8];
	SINT8_t coordTxPower;
	UINT8_t nodeAssocAllowed;
} NetworkInfo;

typedef struct
{
	UINT8_t macAddr[8];
	UINT16_t macShortAddr;
	UINT16_t parentMacShortAddr;
	UINT16_t timeSinceLastRx;
	UINT8_t macCapability;
	UINT16_t pushPeriod;
	UINT8_t radioPartNum[STR_LEN];
	UINT8_t buildDate[STR_LEN];
	UINT8_t buildTime[STR_LEN];
} NodeInfo;

typedef struct
{
	unsigned int rxCnt;
	unsigned int txHdrCnt;
	unsigned int txPylCnt;
	unsigned int crcHdrKo;
	unsigned int crcPylKo;
	unsigned int ackCnt;
	unsigned int nackCnt;
	unsigned int relayFromNode;
} PktStats;

extern UINT16_t calcCkSum16(UINT8_t *buff_p, UINT8_t len);
extern void buildMsgHdr(UINT16_t msgType, UINT8_t *buff_p, UINT16_t pyldLen);
extern void printAckFlags(UINT8_t ackFlag);
extern void printEventType(UINT8_t eventType);
extern const char* getHdrTypeStr(UINT16_t pktType);
extern void printPacket(UINT8_t *pkt, const char *descSuffix, int onlyHdr, int isRxPkt);
extern UINT16_t getRxPayloadSize(UINT16_t type, UINT8_t evtType);
extern UINT8_t isExpectedPyldLen(UINT8_t *rxPkt);
extern void sendHdr(UINT16_t type, UINT8_t *payload, UINT8_t pyldLen);
extern UINT8_t* receivePkt(void);
extern UINT8_t* txHdrRxPkt(UINT16_t type, UINT8_t retries);
extern UINT8_t* txPktRxPkt(UINT16_t type, UINT8_t *payload, UINT8_t pyldLen, UINT8_t retries);
extern UINT8_t processPkt(void *ret_p, UINT8_t *rxPkt);
extern UINT8_t processRelayPkt(UINT8_t *rxPkt);
extern UINT8_t processEvent(UINT8_t *rxPkt);
extern UINT8_t startCoord(void);
extern UINT8_t getNodeInfo(NodeInfo *nodeInfo_p, UINT16_t i);
extern void initAllNodeInfo(void);
extern UINT8_t getNodeWhiteList();
extern UINT8_t getFatalErrId(UINT16_t macShortAddr);
extern void digitalIOCtrl(UINT16_t shortAddr, unsigned int portId, unsigned int pinNr, unsigned int val);
extern void setNodeWhiteList(void);
extern void setNodePushPeriod(UINT16_t macShortAddr, UINT16_t pushPeriod);

extern void __decodeBcnFrame(unsigned char *pdu_p, int pduLen);
extern void __decodeAckFrame(unsigned char *pdu_p, int pduLen);
extern void __decodeCmdFramePyld(unsigned char *pdu_p, int pduLen);
extern void __decodePdu(unsigned char *pdu_p, int pduLen);

#endif /* PKTPROC_H_ */
