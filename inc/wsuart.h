#ifndef __WSUART_H__
#define __WSUART_H__

#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <typedefs.h>

typedef struct
{
  UINT8_t qHead, qTail;
} SYS_trscnQ_s;

#include <uart.h>

typedef struct
{
  int baudRate; 
  int serialFd;
  char *devName_p;
  struct termios dcb;
} cntxt_s;

#define flush(uartCntxt_p)  tcflush((uartCntxt_p)->commDevFd, TCIFLUSH)

extern void setDefaultTimeout();
extern void setTimeout(unsigned int sec, unsigned int ms);
extern int readPort(unsigned char *buff_p, unsigned int len);
extern int writePort(unsigned char *buff_p, unsigned int cnt);
extern int cfgPort(char *devName);

#endif
