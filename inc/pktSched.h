/*
 * pktSched.h
 *
 *  Created on: Nov 21, 2013
 *      Author: AP
 */

#ifndef PKTSCHED_H_
#define PKTSCHED_H_

typedef struct
{
	UINT16_t type;
	UINT16_t len;
	UINT8_t data[512];
} SchedPktInfo;

extern void initSchedTxQueue(void);
extern UINT8_t numPktsQueued(void);
extern void printQueue(const char *desc);
extern UINT8_t scheduleHdr(UINT16_t type);
extern UINT8_t schedulePkt(UINT16_t type, UINT8_t *data, UINT16_t len);
extern UINT8_t scheduleNwInfo(void);
extern UINT8_t processTxQueue(void);

#endif /* PKTSCHED_H_ */
