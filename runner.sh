#!/bin/sh

cd Debug
wsntty=`grep -l PRODUCT=403/6015 /sys/bus/usb-serial/devices/tty*/../uevent | cut -d/ -f6`
while [ 1 ]; do
  running=`ps -e | grep -c wsuart`
  echo $running
  if [ $running -eq 0 ]; then
    echo Starting wsuart ...
    ./wsuart /dev/$wsntty > /dev/null &
  fi
  sleep 5
done

